// All Rights Reserved, Copyright © ScaleFocus

package com.scalefocus.rentacar.mappers;

import com.scalefocus.rentacar.constants.TestConstants;
import com.scalefocus.rentacar.domains.Car;
import com.scalefocus.rentacar.models.binding.CarBindingModel;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class CarMapperTest {

    private CarMapper carMapper;
    private CarBindingModel carBindingModel;

    @Before
    public void setUp() {
        carBindingModel = new CarBindingModel();
        carMapper = CarMapper.INSTANCE;
    }


    @Test
    public void modelToCarMapperTest() {
        Car car;
        List<CarBindingModel> cars = new ArrayList<>();
        carBindingModel.setModel(TestConstants.TEST_STRING);
        car = carMapper.modelToCar(carBindingModel);
        assertEquals(car.getModel(), TestConstants.TEST_STRING);
    }

    @Test
    public void carToModelMapperTest() {
        Car car = new Car();
        car.setModel(TestConstants.TEST_STRING);
        carBindingModel.setModel(TestConstants.TEST_STRING);
        assertEquals(car.getModel(), carBindingModel.getModel());

    }
}