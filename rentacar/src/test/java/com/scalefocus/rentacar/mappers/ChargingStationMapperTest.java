// All Rights Reserved, Copyright © ScaleFocus

package com.scalefocus.rentacar.mappers;

import com.scalefocus.rentacar.constants.TestConstants;
import com.scalefocus.rentacar.domains.ChargingStation;
import com.scalefocus.rentacar.models.binding.ChargingStationBindingModel;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class ChargingStationMapperTest {

    private ChargingStationMapper chargingStationMapper;
    private ChargingStationBindingModel chargingStationBindingModel;

    @Before
    public void setUp() {
        chargingStationBindingModel = new ChargingStationBindingModel();
        chargingStationMapper = ChargingStationMapper.INSTANCE;
    }

    @Test
    public void modelToChargingStationMapperTest() {
        ChargingStation chargingStation;
        List<ChargingStationMapper> chargingStationMappers = new ArrayList<>();
        chargingStationBindingModel.setChargingStationName(TestConstants.TEST_STRING);
        chargingStation = chargingStationMapper.modelToChargingStation(chargingStationBindingModel);
        assertEquals(chargingStation.getChargingStationName(), TestConstants.TEST_STRING);
    }

    @Test
    public void chargingStationMapperToModelMapperTest() {
        ChargingStation chargingStation = new ChargingStation();
        chargingStation.setChargingStationName(TestConstants.TEST_STRING);
        chargingStationBindingModel.setChargingStationName(TestConstants.TEST_STRING);
        assertEquals(chargingStation.getChargingStationName(), chargingStationBindingModel.getChargingStationName());
    }

}