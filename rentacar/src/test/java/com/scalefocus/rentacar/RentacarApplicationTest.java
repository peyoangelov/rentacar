// All Rights Reserved, Copyright © ScaleFocus

package com.scalefocus.rentacar;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RentacarApplicationTest {

    @Test
    public void contextLoads() {
    }

    @Test
    public void main() {
        RentacarApplication.main(new String[]{});
    }
}