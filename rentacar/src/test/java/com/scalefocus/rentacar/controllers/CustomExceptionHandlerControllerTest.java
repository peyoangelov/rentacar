package com.scalefocus.rentacar.controllers;

import com.scalefocus.rentacar.constants.MappingConstants;
import com.scalefocus.rentacar.constants.ViewNameConstants;
import org.junit.Before;
import org.junit.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

public class CustomExceptionHandlerControllerTest {

    MockMvc mockMvc;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        CustomExceptionHandlerController controller = new CustomExceptionHandlerController();
        InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
        viewResolver.setPrefix("/templates/");
        viewResolver.setSuffix(".html");
        mockMvc = MockMvcBuilders.standaloneSetup(controller).setViewResolvers(viewResolver).build();
    }

    @Test
    public void handleError() throws Exception {

        mockMvc.perform(get(MappingConstants.NOT_FOUND_ERROR_PAGE_MAPPING))
                .andExpect(status().isOk())
                .andExpect(view().name(ViewNameConstants.NOT_FOUND_ERROR_PAGE));
    }

    @Test
    public void getErrorPath() throws Exception {
        mockMvc.perform(get(MappingConstants.NOT_FOUND_ERROR_PAGE_MAPPING))
                .andExpect(status().isOk())
                .andExpect(view().name(ViewNameConstants.NOT_FOUND_ERROR_PAGE));
    }
}
