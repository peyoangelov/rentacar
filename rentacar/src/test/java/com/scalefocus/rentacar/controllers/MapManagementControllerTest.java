// All Rights Reserved, Copyright © ScaleFocus

package com.scalefocus.rentacar.controllers;

import com.scalefocus.rentacar.constants.AttributeConstants;
import com.scalefocus.rentacar.constants.Constants;
import com.scalefocus.rentacar.constants.MappingConstants;
import com.scalefocus.rentacar.constants.ViewNameConstants;
import com.scalefocus.rentacar.domains.Car;
import com.scalefocus.rentacar.models.binding.CarBindingModel;
import com.scalefocus.rentacar.services.CarService;
import com.scalefocus.rentacar.services.ChargingStationService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class MapManagementControllerTest {
    @Mock
    private ChargingStationService chargingStationService;
    @Mock
    private CarService carService;

    MockMvc mockMvc;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        MapManagementController controller = new MapManagementController(chargingStationService, carService);
        InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
        viewResolver.setPrefix("/templates/");
        viewResolver.setSuffix(".html");
        mockMvc = MockMvcBuilders.standaloneSetup(controller).setViewResolvers(viewResolver).build();
    }

    @Test
    public void loadAllChargingStations() throws Exception {
        mockMvc.perform(get(MappingConstants.GETS_ALL_CHARGING_STATIONS_MAPPING))
                .andExpect(status().isOk());
        verify(chargingStationService, times(1)).getAllChargingStations();
    }

    @Test
    public void loadAllCarsTestSuccessfulTest() throws Exception {

        mockMvc.perform(get(MappingConstants.GETS_ALL_CARS_MAPPING))
                .andExpect(status().isOk());
        verify(carService, times(1)).getAllCars();
    }


    @Test
    public void updateCarLocation() {
        MapManagementController controller = new MapManagementController(chargingStationService, carService);
        Car car = new Car("BMW","SA",100L,"12.10","12.12");

        assertFalse(controller.updateCarLocation(car.getId(),car.getPowerCapacity().toString(),car.getLatitude(),car.getLongitude()));
    }

    @Test
    public void chargeCarSuccess() {
        MapManagementController controller = new MapManagementController(chargingStationService, carService);
        Car car = new Car("BMW","SA",100L,"12.10","12.12");
        controller.chargeCar(car.getId());
        assertTrue(controller.chargeCar(car.getId()));
    }

}