// All Rights Reserved, Copyright © ScaleFocus

package com.scalefocus.rentacar.services.impl;

import com.scalefocus.rentacar.domains.ChargingStation;
import com.scalefocus.rentacar.mappers.ChargingStationMapper;
import com.scalefocus.rentacar.models.binding.ChargingStationBindingModel;
import com.scalefocus.rentacar.repositories.ChargingStationRepository;
import com.scalefocus.rentacar.services.ChargingStationService;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ChargingStationServiceImpl implements ChargingStationService {

    private final ChargingStationRepository chargingStationRepository;
    private final ChargingStationMapper mapper;

    public ChargingStationServiceImpl(ChargingStationRepository chargingStationRepository) {
        this.mapper = ChargingStationMapper.INSTANCE;
        this.chargingStationRepository = chargingStationRepository;
    }


    @Override
    public List<ChargingStationBindingModel> getAllChargingStations() {
        final List<ChargingStationBindingModel> chargingStations = new ArrayList<>();
        final List<ChargingStation> entities = chargingStationRepository.findAll();
        entities.forEach(station -> chargingStations.add(mapper.chargingStationToModel(station)));

        return chargingStations;
    }
}
