// All Rights Reserved, Copyright © ScaleFocus

package com.scalefocus.rentacar.services;

import com.scalefocus.rentacar.models.binding.CarBindingModel;

import java.util.List;

public interface CarService {

    List<CarBindingModel> getAllCars();

    boolean updateCarLocation(CarBindingModel carBindingModel);

    CarBindingModel getCarBindingModelById(Long id);
}
