// All Rights Reserved, Copyright © ScaleFocus

package com.scalefocus.rentacar.services;

import com.scalefocus.rentacar.models.binding.ChargingStationBindingModel;

import java.util.List;

public interface ChargingStationService {
    List<ChargingStationBindingModel> getAllChargingStations();
}
