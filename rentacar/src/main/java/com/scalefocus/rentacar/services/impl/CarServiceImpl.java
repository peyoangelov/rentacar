// All Rights Reserved, Copyright © ScaleFocus

package com.scalefocus.rentacar.services.impl;

import com.scalefocus.rentacar.domains.Car;
import com.scalefocus.rentacar.mappers.CarMapper;
import com.scalefocus.rentacar.models.binding.CarBindingModel;
import com.scalefocus.rentacar.repositories.CarRepository;
import com.scalefocus.rentacar.services.CarService;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CarServiceImpl implements CarService {
    private final CarRepository carRepository;
    private final CarMapper mapper;

    public CarServiceImpl(CarRepository carRepository) {
        this.mapper = CarMapper.INSTANCE;
        this.carRepository = carRepository;
    }

    /**
     * Gets all cars binding Model.
     *
     * @return - list of all cars binding Model.
     */
    @Override
    public List<CarBindingModel> getAllCars() {
        final List<CarBindingModel> cars = new ArrayList<>();
        final List<Car> entities = this.carRepository.findAll();
        entities.forEach(car -> cars.add(this.mapper.carToModel(car)));
        return cars;
    }

    /**
     * Update car location.
     *
     * @param carBindingModel - of the desire car.
     * @return true if the operation is success.
     */
    public boolean updateCarLocation(CarBindingModel carBindingModel) {
        if (carBindingModel != null) {
            Car car = mapper.modelToCar(carBindingModel);
            carRepository.saveAndFlush(car);
            return true;
        }
        return false;
    }

    /**
     * Get car binding model.
     *
     * @param id of the desire car.
     * @return car binding model.
     */
    @Override
    public CarBindingModel getCarBindingModelById(Long id) {
        Optional<Car> carOptional = carRepository.findById(id);
        if (carOptional.isPresent()) {
            Car car = carOptional.get();
            return mapper.carToModel(car);
        }
        return null;
    }
}
