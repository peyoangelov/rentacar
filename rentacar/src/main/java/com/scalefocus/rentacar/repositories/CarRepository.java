// All Rights Reserved, Copyright © ScaleFocus

package com.scalefocus.rentacar.repositories;

import com.scalefocus.rentacar.domains.Car;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CarRepository extends JpaRepository<Car, Long> {


}
