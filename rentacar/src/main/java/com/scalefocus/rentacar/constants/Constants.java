// All Rights Reserved, Copyright © ScaleFocus

package com.scalefocus.rentacar.constants;

/**
 * Constants class.
 */
public final class Constants {
    public static final Long MAX_POWER_PER_CAR = 150L;
    public static final int MEASURE_UNIT_METERS = 1000;

    private Constants() {
    }
}
