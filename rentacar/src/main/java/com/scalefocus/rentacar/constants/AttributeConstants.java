// All Rights Reserved, Copyright © ScaleFocus

package com.scalefocus.rentacar.constants;

public final class AttributeConstants {

    public static final String CARS = "cars";

    private AttributeConstants() {
    }
}
