// All Rights Reserved, Copyright © ScaleFocus

package com.scalefocus.rentacar.constants;

public final class MappingConstants {

    public static final String NOT_FOUND_ERROR_PAGE_MAPPING = "/error";
    public static final String HOME_PAGE_MAPPING = "/home";
    public static final String GETS_ALL_CHARGING_STATIONS_MAPPING = "/getAllChargingStations";
    public static final String GETS_ALL_CARS_MAPPING = "/getAllCars";
    public static final String UPDATE_CAR_LOCATION = "/updateCarLocation";
    public static final String CHARGE_CAR = "/chargeCar";


    private MappingConstants() {
    }
}
