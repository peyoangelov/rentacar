// All Rights Reserved, Copyright © ScaleFocus

package com.scalefocus.rentacar.constants;

public final class ViewNameConstants {
    public static final String NOT_FOUND_ERROR_PAGE = "/error-page";
    public static final String INDEX_PAGE = "index.html";

    private ViewNameConstants() {
    }
}
