// All Rights Reserved, Copyright © ScaleFocus

package com.scalefocus.rentacar.domains;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "CARS")
public class Car {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CARS_SEQ")
    @SequenceGenerator(sequenceName = "CARS_SEQ", name = "CARS_SEQ", allocationSize = 1)
    @Column(name = "CAR_ID")
    private Long id;

    @Column(name = "MODEL")
    private String model;

    @Column(name = "PLATE")
    private String plate;

    @Column(name = "POWER_CAPACITY")
    private Long powerCapacity;

    @Column(name = "LATITUDE")
    private String latitude;

    @Column(name = "LONGITUDE")
    private String longitude;

    /**
     * Empty Constructor for car entity.
     **/
    public Car() {
    }

    /**
     * Constructor for car entity.
     *
     * @param model         - model of the car.
     * @param plate         - registration plate of the car.
     * @param powerCapacity - power capacity of the car.
     * @param latitude      -of the car.
     * @param longitude     -of the car.
     */
    public Car(String model, String plate, Long powerCapacity, String latitude, String longitude) {
        this.model = model;
        this.plate = plate;
        this.powerCapacity = powerCapacity;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getPlate() {
        return plate;
    }

    public void setPlate(String plate) {
        this.plate = plate;
    }

    public Long getPowerCapacity() {
        return powerCapacity;
    }

    public void setPowerCapacity(Long powerCapacity) {
        this.powerCapacity = powerCapacity;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }
}
