// All Rights Reserved, Copyright © ScaleFocus

package com.scalefocus.rentacar.domains;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "CHARGING_STATIONS")
public class ChargingStation {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CHARGING_STATIONS_SEQ")
    @SequenceGenerator(sequenceName = "CHARGING_STATIONS_SEQ", name = "CHARGING_STATIONS_SEQ", allocationSize = 1)
    @Column(name = "CHARGING_STATION_ID")
    private Long id;

    @Column(name = "CHARGING_STATION_NAME")
    private String chargingStationName;

    @Column(name = "LATITUDE")
    private String latitude;

    @Column(name = "LONGITUDE")
    private String longitude;

    /**
     * Empty constructor of the charging Station.
     */
    public ChargingStation() {
    }

    /**
     * constructor of the charging Station.
     */
    public ChargingStation(String chargingStationName, String latitude, String longitude) {
        this.chargingStationName = chargingStationName;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getChargingStationName() {
        return chargingStationName;
    }

    public void setChargingStationName(String chargingStationName) {
        this.chargingStationName = chargingStationName;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }
}
