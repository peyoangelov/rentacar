// All Rights Reserved, Copyright © ScaleFocus

package com.scalefocus.rentacar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@SuppressWarnings("PMD.UseUtilityClass")
public class RentacarApplication {

    private static final Logger LOGGER = LoggerFactory.getLogger(RentacarApplication.class);

    /**
     * RENTACAR main method.
     *
     * @param args - RENTACAR - starting from this method
     */
    public static void main(String[] args) {

        LOGGER.warn("Application started");
        SpringApplication.run(RentacarApplication.class, args);

    }
}
