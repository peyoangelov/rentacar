// All Rights Reserved, Copyright © ScaleFocus

package com.scalefocus.rentacar.controllers;

import com.scalefocus.rentacar.constants.MappingConstants;
import com.scalefocus.rentacar.constants.ViewNameConstants;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Custom exception controller.
 */
@Controller
public class CustomExceptionHandlerController implements ErrorController {
    private static final Logger LOGGER = LoggerFactory.getLogger(CustomExceptionHandlerController.class);

    @RequestMapping(MappingConstants.NOT_FOUND_ERROR_PAGE_MAPPING)
    public String handleError() {
        LOGGER.info("Error occurred!");
        return ViewNameConstants.NOT_FOUND_ERROR_PAGE;
    }

    @Override
    public String getErrorPath() {
        return ViewNameConstants.NOT_FOUND_ERROR_PAGE;
    }


}
