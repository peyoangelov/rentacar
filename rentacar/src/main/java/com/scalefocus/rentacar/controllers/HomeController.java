// All Rights Reserved, Copyright © ScaleFocus

package com.scalefocus.rentacar.controllers;

import com.scalefocus.rentacar.constants.MappingConstants;
import com.scalefocus.rentacar.constants.ViewNameConstants;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * Home Controller.
 */
@Controller
public class HomeController {

    /**
     * Shows the map.
     *
     * @return model and view with the map.
     */
    @GetMapping(MappingConstants.HOME_PAGE_MAPPING)
    public ModelAndView showMap() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName(ViewNameConstants.INDEX_PAGE);
        return modelAndView;
    }
}
