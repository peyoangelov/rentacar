// All Rights Reserved, Copyright © ScaleFocus

package com.scalefocus.rentacar.controllers;

import com.scalefocus.rentacar.constants.Constants;
import com.scalefocus.rentacar.constants.MappingConstants;
import com.scalefocus.rentacar.models.binding.CarBindingModel;
import com.scalefocus.rentacar.models.binding.ChargingStationBindingModel;
import com.scalefocus.rentacar.services.CarService;
import com.scalefocus.rentacar.services.ChargingStationService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Controller for management of the map.
 */
@RestController
public class MapManagementController {
    private static final Logger LOGGER = LoggerFactory.getLogger(MapManagementController.class);


    private final ChargingStationService chargingStationService;
    private final CarService carService;

    /**
     * Constructor for Management map.
     *
     * @param chargingStationService - charging station.
     * @param carService             car.
     */
    @Autowired
    public MapManagementController(ChargingStationService chargingStationService, CarService carService) {
        this.chargingStationService = chargingStationService;
        this.carService = carService;
    }

    /**
     * Loads all charging stations.
     *
     * @return - all charging stations.
     */
    @GetMapping(MappingConstants.GETS_ALL_CHARGING_STATIONS_MAPPING)
    public List<ChargingStationBindingModel> loadAllChargingStations() {
        return chargingStationService.getAllChargingStations();
    }

    /**
     * Loads all cars.
     *
     * @return - all cars.
     */
    @GetMapping(MappingConstants.GETS_ALL_CARS_MAPPING)
    public List<CarBindingModel> loadAllCars() {
        return carService.getAllCars();
    }

    /**
     * Update car location on the map.
     *
     * @param carId     - car.
     * @param latitude  - latitude of the car.
     * @param longitude - longitude of the car.
     * @param distance  - distance of travel.
     * @return - true if location is updated.
     */
    @PostMapping(MappingConstants.UPDATE_CAR_LOCATION)
    public boolean updateCarLocation(final Long carId, final String latitude, final String longitude,
                                     final String distance) {
        try {
            final CarBindingModel carBindingModel = carService.getCarBindingModelById(carId);
            boolean isModelChanged = false;
            if (carBindingModel != null) {
                if (latitude != null) {
                    carBindingModel.setLatitude(latitude);
                    isModelChanged = true;
                }
                if (longitude != null) {
                    carBindingModel.setLongitude(longitude);
                    isModelChanged = true;
                }
                if (distance != null) {
                    isModelChanged = setPowerCapacityBasedOnDistance(distance, carBindingModel);
                }
                if (isModelChanged) {
                    carService.updateCarLocation(carBindingModel);
                }
                return true;
            }
            return false;
        } catch (Exception e) {
            LOGGER.info(e.getMessage());
            return false;
        }
    }

    /**
     * Set the new power based on distance travelled.
     *
     * @param distance         - traveled.
     * @param carBindingModel- car.
     * @return true if its ok.
     */
    private boolean setPowerCapacityBasedOnDistance(final String distance, final CarBindingModel carBindingModel) {
        if (Double.parseDouble(distance) == 0) {
            carBindingModel.setPowerCapacity(Constants.MAX_POWER_PER_CAR);
        } else {
            final Long updatedPowerCapacity = updatePowerCapacity(distance,
                    carBindingModel.getPowerCapacity());
            if (updatedPowerCapacity < 0) {
                return false;
            }
            carBindingModel.setPowerCapacity(updatedPowerCapacity);
        }
        return true;
    }

    /**
     * Charging the car with power.
     *
     * @param carId - car
     * @return true if its correct.
     */
    @PostMapping(MappingConstants.CHARGE_CAR)
    public boolean chargeCar(final Long carId) {
        try {
            CarBindingModel carBindingModel = carService.getCarBindingModelById(carId);
            if (carBindingModel != null) {
                carBindingModel.setPowerCapacity(Constants.MAX_POWER_PER_CAR);
                carService.updateCarLocation(carBindingModel);
            }
            return true;
        } catch (Exception e) {
            LOGGER.info(e.getMessage());
            return false;
        }
    }

    /**
     * update the power capacity.
     *
     * @param distance    - distance.
     * @param oldCapacity - old car power.
     * @return - the new power capacity.
     */
    private Long updatePowerCapacity(final String distance, final Long oldCapacity) {
        final Double distanceInKm = Double.parseDouble(distance) / Constants.MEASURE_UNIT_METERS;
        return oldCapacity - distanceInKm.longValue();
    }

}
