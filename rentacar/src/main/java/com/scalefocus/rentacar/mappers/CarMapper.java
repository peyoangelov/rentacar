// All Rights Reserved, Copyright © ScaleFocus

package com.scalefocus.rentacar.mappers;

import com.scalefocus.rentacar.domains.Car;
import com.scalefocus.rentacar.models.binding.CarBindingModel;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface CarMapper {

    CarMapper INSTANCE = Mappers.getMapper(CarMapper.class);

    Car modelToCar(CarBindingModel model);

    CarBindingModel carToModel(Car car);
}