// All Rights Reserved, Copyright © ScaleFocus

package com.scalefocus.rentacar.mappers;

import com.scalefocus.rentacar.domains.ChargingStation;
import com.scalefocus.rentacar.models.binding.ChargingStationBindingModel;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface ChargingStationMapper {
    ChargingStationMapper INSTANCE = Mappers.getMapper(ChargingStationMapper.class);

    ChargingStation modelToChargingStation(ChargingStationBindingModel model);

    ChargingStationBindingModel chargingStationToModel(ChargingStation chargingStation);
}
