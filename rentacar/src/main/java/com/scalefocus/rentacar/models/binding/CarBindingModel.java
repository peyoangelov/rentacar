// All Rights Reserved, Copyright © ScaleFocus

package com.scalefocus.rentacar.models.binding;

public class CarBindingModel {

    private Long id;
    private String model;
    private String plate;
    private Long powerCapacity;
    private String latitude;
    private String longitude;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getPlate() {
        return plate;
    }

    public void setPlate(String plate) {
        this.plate = plate;
    }

    public Long getPowerCapacity() {
        return powerCapacity;
    }

    public void setPowerCapacity(Long powerCapacity) {
        this.powerCapacity = powerCapacity;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }
}
