var start_location;
var map;
var isReservationActive = false;
var reservedCar;
(function () {
    loadMap();
    renderEventListeners();
})();

function renderEventListeners() {
    $('#view-switch').on('click', function () {
        if ($(this).hasClass('satellite')) {
            map.setStyle('mapbox://styles/peyoangelov/cjtjryfeg25711fquubbv3udm');
            $(this).removeClass('satellite');
            $('#view-switch').removeClass('satellite');
            $('#header').removeClass('satellite');
        } else {
            map.setStyle('mapbox://styles/mapbox/satellite-v9');
            $(this).addClass('satellite');
            $('#view-switch').addClass('satellite');
            $('#header').addClass('satellite');
        }
    });

    if (getCookie('acceptInstructions') == '') {
        $("#cookie-box").fadeIn("slow");
    }

    $('#js-accept-terms').on('click', function () {
        $("#cookie-box").fadeOut("slow");
        setCookie('acceptInstructions', true, 30);
    });

    $('#car-locator').on('click', function () {
        $('#car-locator-action').fadeIn('slow');
    })

    $('#info').on('click', function () {
        $('#info-box').fadeIn('slow');
    });

    $('#js-close-alert-box').on('click', function () {
        $('#info-box').fadeOut('slow');
    });

    $('#close-car-locator').on('click', function () {
        $('#car-locator-action').fadeOut('slow');
    })


}

function loadMap() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(openCurrentLocation, openDefaultLocation);
    } else {
        openDefaultLocation();
    }
}

function openCurrentLocation(position) {
    var lat = position.coords.latitude;
    var lon = position.coords.longitude;
    start_location = [lon, lat];
    openMap(start_location, 12);
}

function openDefaultLocation() {
    start_location = [23.32, 42.697]; //Default value  Sofia  coordinates
    openMap(start_location, 12);
}

function openMap(start_location, zoom) {
    if (start_location != null) {
        mapboxgl.accessToken = 'pk.eyJ1IjoicGV5b2FuZ2Vsb3YiLCJhIjoiY2p0anJpcjg2MDF5NjQ5bWpsOXJiMnkyOCJ9.mF7cEmpBdXRidvGCokZfEw';

        if (!mapboxgl.supported()) {
            alert('Your browser does not support Mapbox GL');
        } else {
            map = new mapboxgl.Map({
                container: 'map',
                style: 'mapbox://styles/peyoangelov/cjtjryfeg25711fquubbv3udm',
                center: start_location,
                zoom: zoom,
                minZoom: 2,
                maxZoom: 18,
                bearing: 0,
                pitch: 0,
                attributionControl: false
            });
            getAllChargingStations();
            getAllCars();
            selectFinalDestination();
        }
    }
}

function getAllChargingStations() {
    $.ajax({
        url: '/getAllChargingStations',
        type: 'GET',
        contentType: 'json',
        success: function (response) {
            response.forEach(function (marker) {
                var element = document.createElement("div");
                element.className = "station";
                // create the popup with station name
                var popup = new mapboxgl.Popup({offset: 25})
                    .setText(marker.chargingStationName);
                new mapboxgl.Marker(element).setLngLat([marker.longitude, marker.latitude]).setPopup(popup).addTo(map);
            });
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log(errorThrown);
        }
    });
}

function reserveCar(carID) {
    $('#alert-box').stop();
    $('#alert-box').text('Click to select final destination');
    $('#alert-box').fadeIn("slow").fadeOut(5000);
    $('#' + carID).addClass('active');
    isReservationActive = true;
    reservedCar = $('#' + carID);
}

function reportCar(carID) {
    $('html').addClass('preloader');
    $.ajax({
        url: '/chargeCar',
        type: 'POST',
        dataType: 'json',
        data: {
            carId: carID
        },
        success: function (response) {
            $('.car').remove();
            $('.mapboxgl-popup').remove();
            getAllCars();
            setTimeout(function(){$('html').removeClass('preloader'); }, 2000);
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log(errorThrown);
            setTimeout(function(){$('html').removeClass('preloader'); }, 2000);
        }
    });
}

function chargeCarOnStation(pointLng, pointLat) {
    var activeCar = $('.car.active');
    updateCarLocation(activeCar.attr('id'), pointLng, pointLat, 0);
}

function goToEndpoint(pointLng, pointLat) {
    isReservationActive = false;
    var activeCar = $('.car.active');
    var end = [pointLng, pointLat];
    var url = 'https://api.mapbox.com/directions/v5/mapbox/driving/' + activeCar.data('longitude') + ',' + activeCar.data('latitude') + ';' + end[0] + ',' + end[1] + '?steps=true&geometries=geojson&access_token=' + mapboxgl.accessToken;

    // make an XHR request https://developer.mozilla.org/en-US/docs/Web/API/XMLHttpRequest
    var req = new XMLHttpRequest();
    req.responseType = 'json';
    req.open('GET', url, true);
    req.onload = function () {
        var data = req.response.routes[0];
        var distance = data.distance;
        if (checkIfDistanceIsReachable(distance, activeCar.data('power'))) {
            var route = data.geometry.coordinates;
            var geojson = {
                type: 'Feature',
                properties: {},
                geometry: {
                    type: 'LineString',
                    coordinates: route
                }
            };
            // if the route already exists on the map, reset it using setData
            if (map.getSource('route')) {
                map.getSource('route').setData(geojson);
            } else { // otherwise, make a new request
                map.addLayer({
                    id: 'route',
                    type: 'line',
                    source: {
                        type: 'geojson',
                        data: {
                            type: 'Feature',
                            properties: {},
                            geometry: {
                                type: 'LineString',
                                coordinates: geojson
                            }
                        }
                    },
                    layout: {
                        'line-join': 'round',
                        'line-cap': 'round'
                    },
                    paint: {
                        'line-color': '#da2427',
                        'line-width': 5,
                        'line-opacity': 0.75
                    }
                });
            }
            $('#alert-box').stop();
            $('#alert-box').text("Your price is only: "+ (distance /1000).toPrecision(2)+ " BGN!");
            $('#alert-box').fadeIn('slow').fadeOut(5000);
            updateCarLocation(activeCar.attr('id'), pointLng, pointLat, distance);
        } else {
            $('#alert-box').stop();
            $('#alert-box').text("The car does not have enough power! Please, visit a charging station!");
            $('#alert-box').fadeIn('slow').fadeOut(5000);
        }
    }
    req.send();
}

function findCar(lng, lat) {
    map.flyTo({center: {'lng': lng, 'lat': lat}});
    $('#car-locator-action').fadeOut('slow');
}

function getAllCars() {
    $.ajax({
        url: '/getAllCars',
        type: 'GET',
        contentType: 'json',
        success: function (response) {
            $('#car-list').html('');
            response.forEach(function (marker) {
                var element = document.createElement("div");
                element.className = "car";
                element.id = marker.id;
                element.setAttribute("data-power", marker.powerCapacity);
                element.setAttribute("data-longitude", marker.longitude);
                element.setAttribute("data-latitude", marker.latitude);
                $(element).on('click', function () {
                    $('.car').removeClass('active');
                })

                // create the popup
                var popup;
                if (marker.powerCapacity <= 10) {
                    popup = new mapboxgl.Popup({offset: 25})
                        .setHTML('Oops, you can not reserve this car, as it was left with no power. <br/> Call support by clicking the button! <br/>  <button class="rent-car" onclick="reportCar(' + marker.id + ');" data-car-id="' + marker.id + '">Call us!</button>');
                } else {
                    popup = new mapboxgl.Popup({offset: 25})
                        .setHTML('Model:' + marker.model + ' <br/> Power left: ' + marker.powerCapacity
                            + 'km <br/>  Price: 1 BGN/KM <br/>  <button class="rent-car" onclick="reserveCar(' + marker.id + ');" data-car-id="' + marker.id + '">Rent</button>');
                }
                new mapboxgl.Marker(element).setLngLat([marker.longitude, marker.latitude]).setPopup(popup).addTo(map);
                $('.mapboxgl-popup-close-button').on('click', function () {
                    $('.endpoint').remove();
                })


                // render car locator menu
                var menuEntry = '<div class="menu-entry-holder"><div class="menu-entry">' + marker.model + '</div>' +
                    '<div class="menu-entry"><button class="rent-car" onclick="findCar(' + marker.longitude + ',' + marker.latitude + ')">Find</button></div></div>';
                $('#car-list').append(menuEntry);
            });
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log(errorThrown);
        }
    });
}

function selectFinalDestination() {
    map.on('click', function (e) {
        var coordinates = e.lngLat;

        if (isReservationActive && $(e.originalEvent.target).hasClass('station')) {

            new mapboxgl.Popup({offset: 25})
                .setLngLat(coordinates)
                .setHTML('<button class="confirm-endpoint" onclick="chargeCarOnStation(' + coordinates.lng + ',' + coordinates.lat + ');">Come and charge your car here!</button>')
                .addTo(map);

        } else if (isReservationActive && !$(e.originalEvent.target).hasClass('car')) {
            map.flyTo({center: coordinates});

            $('.endpoint').remove();
            var element = document.createElement("div");
            element.className = "endpoint";

            new mapboxgl.Marker(element).setLngLat(coordinates).addTo(map);
            new mapboxgl.Popup({offset: 25})
                .setLngLat(coordinates)
                .setHTML('<button class="confirm-endpoint" onclick="goToEndpoint(' + coordinates.lng + ',' + coordinates.lat + ');">Confirm</button>')
                .addTo(map);
        }
    });
}

function checkIfDistanceIsReachable(distance, power) {
    var powerMeters = power * 1000;
    return powerMeters - distance >= 10000;
}

function updateCarLocation(carId, longitude, latitude, distance) {
    $('html').addClass('traveling');
    $.ajax({
        url: '/updateCarLocation',
        type: 'POST',
        dataType: 'json',
        data: {
            carId: carId,
            longitude: longitude,
            latitude: latitude,
            distance: distance
        },
        success: function (response) {
            if (response) {
                $('.car').remove();
                getAllCars();
                $('.endpoint').remove();
                $('.mapboxgl-popup').remove();
                setTimeout(function () {
                    $('html').removeClass('traveling');
                }, 2000);
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log(errorThrown);
            setTimeout(function () {
                $('html').removeClass('traveling');
            }, 2000);
        }
    })
}


function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}
